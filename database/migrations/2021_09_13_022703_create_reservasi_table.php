<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi', function (Blueprint $table) {
            $table->id();
            $table->timestamp('waktu_pesan');
            $table->timestamps();
        });

        Schema::table('reservasi', function (Blueprint $table) {
            $table->unsignedBigInteger('id_penonton');
            $table->unsignedBigInteger('id_film');
            $table->foreign('id_penonton')->references('id')->on('penonton');
            $table->foreign('id_film')->references('id')->on('film');

            $table->unique(['id_penonton', 'id_film']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi');
    }
}
