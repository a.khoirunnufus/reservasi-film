<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    use HasFactory;

    protected $table = 'penonton';
    protected $fillable = ['nama', 'jenis_kelamin', 'email'];

    public function reservasi()
    {
        return $this->hasMany(Reservasi::class, 'id_penonton');
    }
}
