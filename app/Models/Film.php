<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'film';
    protected $fillable = ['judul', 'mulai_tayang', 'selesai_tayang', 'keterangan'];

    public function reservasi()
    {
        return $this->hasMany(Reservasi::class, 'id_film');
    }
}
