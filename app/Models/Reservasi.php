<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    use HasFactory;

    protected $table = 'reservasi';
    protected $fillable = ['id_penonton', 'id_film', 'waktu_pesan'];

    public function penonton()
    {
        return $this->belongsTo(Penonton::class, 'id_penonton');
    }

    public function film()
    {
        return $this->belongsTo(Film::class, 'id_film');
    }
}
