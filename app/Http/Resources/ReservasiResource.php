<?php

namespace App\Http\Resources;

use App\Models\Film;
use App\Models\Penonton;
use Illuminate\Http\Resources\Json\JsonResource;

class ReservasiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'penonton' => PenontonResource::make(Penonton::find($this->id_penonton)),
            'film' => FilmResource::make(Film::find($this->id_film)),
            'waktu_pesan' => $this->waktu_pesan
        ];
    }
}
