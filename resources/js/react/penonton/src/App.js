import React, { useState, useEffect, useRef } from 'react';
import { Button, Alert } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';

import PenontonTable from './components/PenontonTable';
import FormModal from './components/FormModal';
import DeleteModal from './components/DeleteModal';

function useDidMount() {
  const didMountRef = useRef(true);
  
  useEffect(() => {
    didMountRef.current = false;
  }, []);
  return didMountRef.current;
};

function App() {
  const didMount = useDidMount();

  // penonton data
  const [penonton, setPenonton] = useState([]);

  // form field
  const [nama, setNama] = useState("");
  const [jenisKelamin, setJenisKelamin] = useState("");
  const [email, setEmail] = useState("");
  const [updateId, setUpdateId] = useState(0);
  const [deleteId, setDeleteId] = useState(0);

  // form modal
  const [showFormModal, setShowFormModal] = useState(false);
  const [formType, setFormType] = useState('');
  const handleCloseFormModal = () => {
    setNama('');
    setJenisKelamin('');
    setEmail('');
    setFormType('');
    setShowFormModal(false);
  };

  // delete modal
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const handleCloseDeleteModal = () => setShowDeleteModal(false);
  const handleShowDeleteModal = () => setShowDeleteModal(true);

  // alert
  const [showAlert, setShowAlert] = useState(false);
  const [alertType, setAlertType] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  
  useEffect(() => {
    if(didMount) {
      // action when mount
      updatePenontonData();
    } 
  });
  
  const updatePenontonData = () => {
    fetchPenonton()
    .then(data => {
      setPenonton(data.data);
    }) 
    .catch(err => {
      console.error(err);
    });
  }

  const fetchPenonton = () => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/penonton')
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  const fetchSinglePenonton = (id) => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/penonton/'+id)
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err));
    })
  }

  // insert penonton
  const handleShowFormInsertModal = () => {
    setFormType('insert');
    setShowFormModal(true);
  }
  const insertPenonton = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/penonton',
        {
          method: 'POST',
          body: JSON.stringify({nama, jenis_kelamin: jenisKelamin, email}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormInsert = () => {
    insertPenonton()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menambahkan Data');
        setShowAlert(true);

        // update penonton data
        updatePenontonData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menambahkan Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // update penonton
  const handleShowFormUpdateModal = (id) => {
    fetchSinglePenonton(id)
      .then(data => {
        // set form data
        setNama(data.data.nama);
        setJenisKelamin(data.data.jenis_kelamin);
        setEmail(data.data.email);
        setUpdateId(id);
        setFormType('update');

        // show form
        setShowFormModal(true);
      })
      .catch(err => console.error(err))
  }
  const updatePenonton = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/penonton/'+updateId,
        {
          method: 'PUT',
          body: JSON.stringify({nama, jenis_kelamin: jenisKelamin, email}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormUpdate = () => {
    updatePenonton()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Mengupdate Data');
        setShowAlert(true);

        // update penonton data
        updatePenontonData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Mengupdate Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // delete penonton  
  const deletePenonton = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/penonton/'+deleteId,
        {
          method: 'DELETE'
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormDelete = () => {
    deletePenonton()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menghapus Data');
        setShowAlert(true);

        // update penonton data
        updatePenontonData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menghapus Data');
        setShowAlert(true);
      });
    handleCloseDeleteModal();
  }

  return (
    <div className="App">
      <h3 className="mb-3"><u>Penonton</u></h3>
      
      <Button variant="primary mb-3 d-block ms-auto" onClick={handleShowFormInsertModal}>
        <FaPlus /> Tambah
      </Button>

      <PenontonTable 
        penonton={penonton}
        handleShowFormUpdateModal={handleShowFormUpdateModal}
        handleShowDeleteModal={handleShowDeleteModal}
        setDeleteId={setDeleteId}
      />

      <FormModal 
        showFormModal={showFormModal}
        handleCloseFormModal={handleCloseFormModal}
        formType={formType}
        nama={nama}
        email={email}
        jenisKelamin={jenisKelamin}
        setNama={setNama}
        setJenisKelamin={setJenisKelamin}
        setEmail={setEmail}
        handleFormInsert={handleFormInsert}
        handleFormUpdate={handleFormUpdate}
      />

      <DeleteModal 
        showDeleteModal={showDeleteModal}
        handleCloseDeleteModal={handleCloseDeleteModal}
        handleFormDelete={handleFormDelete}
      />

      <Alert style={{position: 'fixed', top: '1rem', right: '1rem'}} show={showAlert} variant={alertType} onClose={() => setShowAlert(false)} dismissible>
        <span>{alertMessage}</span>
      </Alert>

    </div>
  );
}

export default App;
