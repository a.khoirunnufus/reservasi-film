import { Form, Modal, Button } from 'react-bootstrap';

const FormModal = (props) => {
    const {
      showFormModal,
      handleCloseFormModal,
      formType,
      nama,
      email,
      jenisKelamin,
      setNama,
      setJenisKelamin,
      setEmail,
      handleFormInsert,
      handleFormUpdate
    } = props;
  
    return (
      <Modal show={showFormModal} onHide={handleCloseFormModal}>
        <Modal.Header closeButton>
          <Modal.Title>{ formType === 'insert' ? 'Tambah' : 'Update'} Penonton</Modal.Title>
        </Modal.Header>
  
        <Modal.Body>
          
        <Form>
          <Form.Group className="mb-3" controlId="formNama">
            <Form.Label>Nama</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Masukkan Nama"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formJKelamin">
            <Form.Label>Jenis Kelamin</Form.Label>
            <Form.Select
              value={jenisKelamin}
              onChange={(e) => setJenisKelamin(e.target.value)}
            >
              <option value="none">Pilih Jenis Kelamin</option>
              <option value="Laki-Laki">Laki-Laki</option>
              <option value="Perempuan">Perempuan</option>
            </Form.Select>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control 
              type="email" 
              placeholder="Masukkan Email" 
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
        </Form>
  
        </Modal.Body>
  
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseFormModal}>Batal</Button>
          { 
            formType === 'insert' ? 
              <Button variant="primary" onClick={handleFormInsert}>Tambah</Button>
              : <Button variant="warning" onClick={handleFormUpdate}>Update</Button>
          }
        </Modal.Footer>
      </Modal>
    )
}

export default FormModal;