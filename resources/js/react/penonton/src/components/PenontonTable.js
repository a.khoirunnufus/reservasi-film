import { Table, Button } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';

const PenontonTable = (props) => {
    const {
        penonton,
        handleShowFormUpdateModal,
        handleShowDeleteModal,
        setDeleteId
    } = props;

    return (
        <Table striped bordered hover style={{width: '100%'}}>
            <thead>
                <tr>
                <th style={{textAlign: 'center', width: '30px'}}>#</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Email</th>
                <th style={{textAlign: 'center', width: '90px'}}>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {
                penonton.length > 0 &&
                    penonton.map((item, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.nama}</td>
                        <td>{item.jenis_kelamin}</td>
                        <td>{item.email}</td>
                        <td>
                        <Button 
                            onClick={() => handleShowFormUpdateModal(item.id)} 
                            className="me-2" 
                            variant="warning" 
                            size="sm"
                        ><FaEdit /></Button>
                        
                        <Button onClick={() => {
                            handleShowDeleteModal();
                            setDeleteId(item.id);
                            }} 
                            variant="danger" 
                            size="sm"
                        ><FaTrash /></Button>
                        </td>
                    </tr>
                    ))
                }
            </tbody>
        </Table>
    )
}

export default PenontonTable;