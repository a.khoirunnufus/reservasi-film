import { Modal, Button } from 'react-bootstrap';

const DeleteModal = (props) => {
    const {
      showDeleteModal,
      handleCloseDeleteModal,
      handleFormDelete
    } = props;
  
    return (
      <Modal show={showDeleteModal} onHide={handleCloseDeleteModal}>
        <Modal.Header closeButton>
          <Modal.Title>Hapus Reservasi</Modal.Title>
        </Modal.Header>
  
        <Modal.Body>
          
        <h5>Anda yakin ingin menghapus data ini?</h5>
  
        </Modal.Body>
  
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDeleteModal}>Batal</Button>
          <Button variant="danger" onClick={handleFormDelete}>Hapus</Button>
        </Modal.Footer>
      </Modal>
    )
}

export default DeleteModal;