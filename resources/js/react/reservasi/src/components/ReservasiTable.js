import { Table, Button } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';

const ReservasiTable = (props) => {
    const {
        reservasi,
        handleShowFormUpdateModal,
        handleShowDeleteModal,
        setDeleteId
    } = props;

    return (
        <Table striped bordered hover style={{width: '100%'}}>
            <thead>
                <tr>
                <th style={{textAlign: 'center', width: '30px'}}>#</th>
                <th>Penonton</th>
                <th>Film</th>
                <th>Waktu Pesan</th>
                <th style={{textAlign: 'center', width: '90px'}}>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {
                reservasi.length > 0 &&
                    reservasi.map((item, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.penonton.nama}</td>
                        <td>{item.film.judul}</td>
                        <td>{item.waktu_pesan}</td>
                        <td>
                        <Button 
                            onClick={() => handleShowFormUpdateModal(item.id)} 
                            className="me-2" 
                            variant="warning" 
                            size="sm"
                        ><FaEdit /></Button>
                        
                        <Button onClick={() => {
                            handleShowDeleteModal();
                            setDeleteId(item.id);
                            }} 
                            variant="danger" 
                            size="sm"
                        ><FaTrash /></Button>
                        </td>
                    </tr>
                    ))
                }
            </tbody>
        </Table>
    )
}

export default ReservasiTable;