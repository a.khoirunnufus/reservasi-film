import React, { useState, useEffect, useRef } from 'react';
import { Form, Modal, Button } from 'react-bootstrap';

function useDidMount() {
  const didMountRef = useRef(true);
  
  useEffect(() => {
    didMountRef.current = false;
  }, []);
  return didMountRef.current;
};

const FormModal = (props) => {
  const didMount = useDidMount();

  const [penonton, setPenonton] = useState([]);
  const [film, setFilm] = useState([]);

  const {
    showFormModal,
    handleCloseFormModal,
    formType,
    idPenonton,
    waktuPesan,
    idFilm,
    setIdPenonton,
    setIdFilm,
    setWaktuPesan,
    handleFormInsert,
    handleFormUpdate
  } = props;

  useEffect(() => {
    if(didMount) {
      // fetch peonton and film
      fetch('http://localhost:8000/api/penonton')
        .then(res => res.json())
        .then(data => setPenonton(data.data))
        .catch(err => console.error(err));

      fetch('http://localhost:8000/api/film')
        .then(res => res.json())
        .then(data => setFilm(data.data))
        .catch(err => console.error(err));
    } 
  });

  return (
    <Modal show={showFormModal} onHide={handleCloseFormModal}>
      <Modal.Header closeButton>
        <Modal.Title>{ formType === 'insert' ? 'Tambah' : 'Update'} Reservasi</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        
      <Form>
        <Form.Group className="mb-3" controlId="formIdPenonton">
          <Form.Label>Penonton</Form.Label>
          <Form.Select 
            value={idPenonton}
            onChange={(e) => setIdPenonton(e.target.value)}
          >
            <option value='' disabled>Pilih Penonton</option>
            {
              penonton.map((item, index) => (
                <option key={index} value={item.id}>{item.nama}</option>
              ))
            }
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formIdFilm">
          <Form.Label>Film</Form.Label>
          <Form.Select 
            value={idFilm}
            onChange={(e) => setIdFilm(e.target.value)}
          >
            <option value='' disabled>Pilih Film</option>
            {
              film.map((item, index) => (
                <option key={index} value={item.id}>{item.judul}</option>
              ))
            }
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formWaktuPesan">
          <Form.Label>Waktu Pesan</Form.Label>
          <Form.Control 
            type="waktuPesan" 
            placeholder="Masukkan Waktu Pesan" 
            value={waktuPesan}
            onChange={(e) => setWaktuPesan(e.target.value)}
          />
        </Form.Group>
      </Form>

      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={handleCloseFormModal}>Batal</Button>
        { 
          formType === 'insert' ? 
            <Button variant="primary" onClick={handleFormInsert}>Tambah</Button>
            : <Button variant="warning" onClick={handleFormUpdate}>Update</Button>
        }
      </Modal.Footer>
    </Modal>
  )
}

export default FormModal;