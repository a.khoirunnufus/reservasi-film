import React, { useState, useEffect, useRef } from 'react';
import { Button, Alert } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';

import ReservasiTable from './components/ReservasiTable';
import FormModal from './components/FormModal';
import DeleteModal from './components/DeleteModal';

function useDidMount() {
  const didMountRef = useRef(true);
  
  useEffect(() => {
    didMountRef.current = false;
  }, []);
  return didMountRef.current;
};

function App() {
  const didMount = useDidMount();

  // reservasi data
  const [reservasi, setReservasi] = useState([]);

  // form field
  const [idPenonton, setIdPenonton] = useState("");
  const [idFilm, setIdFilm] = useState("");
  const [waktuPesan, setWaktuPesan] = useState("");
  const [updateId, setUpdateId] = useState(0);
  const [deleteId, setDeleteId] = useState(0);

  // form modal
  const [showFormModal, setShowFormModal] = useState(false);
  const [formType, setFormType] = useState('');
  const handleCloseFormModal = () => {
    setIdPenonton('');
    setIdFilm('');
    setWaktuPesan('');
    setFormType('');
    setShowFormModal(false);
  };

  // delete modal
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const handleCloseDeleteModal = () => setShowDeleteModal(false);
  const handleShowDeleteModal = () => setShowDeleteModal(true);

  // alert
  const [showAlert, setShowAlert] = useState(false);
  const [alertType, setAlertType] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  
  useEffect(() => {
    if(didMount) {
      // action when mount
      updateReservasiData();
    } 
  });
  
  const updateReservasiData = () => {
    fetchReservasi()
    .then(data => {
      setReservasi(data.data);
    }) 
    .catch(err => {
      console.error(err);
    });
  }

  const fetchReservasi = () => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/reservasi')
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  const fetchSingleReservasi = (id) => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/reservasi/'+id)
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err));
    })
  }

  // insert reservasi
  const handleShowFormInsertModal = () => {
    setFormType('insert');
    setShowFormModal(true);
  }
  const insertReservasi = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/reservasi',
        {
          method: 'POST',
          body: JSON.stringify({id_penonton: idPenonton, id_film: idFilm, waktu_pesan: waktuPesan}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormInsert = () => {
    insertReservasi()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menambahkan Data');
        setShowAlert(true);

        // update reservasi data
        updateReservasiData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menambahkan Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // update reservasi
  const handleShowFormUpdateModal = (id) => {
    fetchSingleReservasi(id)
      .then(data => {
        // set form data
        setIdPenonton(data.data.penonton.id);
        setIdFilm(data.data.film.id);
        setWaktuPesan(data.data.waktu_pesan);
        setUpdateId(id);
        setFormType('update');

        // show form
        setShowFormModal(true);
      })
      .catch(err => console.error(err))
  }
  const updateReservasi = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/reservasi/'+updateId,
        {
          method: 'PUT',
          body: JSON.stringify({id_penonton: idPenonton, id_film: idFilm, waktu_pesan: waktuPesan}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormUpdate = () => {
    updateReservasi()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Mengupdate Data');
        setShowAlert(true);

        // update reservasi data
        updateReservasiData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Mengupdate Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // delete reservasi  
  const deleteReservasi = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/reservasi/'+deleteId,
        {
          method: 'DELETE'
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormDelete = () => {
    deleteReservasi()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menghapus Data');
        setShowAlert(true);

        // update reservasi data
        updateReservasiData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menghapus Data');
        setShowAlert(true);
      });
    handleCloseDeleteModal();
  }

  return (
    <div className="App">
      <h3 className="mb-3"><u>Reservasi</u></h3>
      
      <Button variant="primary mb-3 d-block ms-auto" onClick={handleShowFormInsertModal}>
        <FaPlus /> Tambah
      </Button>

      <ReservasiTable 
        reservasi={reservasi}
        handleShowFormUpdateModal={handleShowFormUpdateModal}
        handleShowDeleteModal={handleShowDeleteModal}
        setDeleteId={setDeleteId}
      />

      <FormModal 
        showFormModal={showFormModal}
        handleCloseFormModal={handleCloseFormModal}
        formType={formType}
        idPenonton={idPenonton}
        waktuPesan={waktuPesan}
        idFilm={idFilm}
        setIdPenonton={setIdPenonton}
        setIdFilm={setIdFilm}
        setWaktuPesan={setWaktuPesan}
        handleFormInsert={handleFormInsert}
        handleFormUpdate={handleFormUpdate}
      />

      <DeleteModal 
        showDeleteModal={showDeleteModal}
        handleCloseDeleteModal={handleCloseDeleteModal}
        handleFormDelete={handleFormDelete}
      />

      <Alert style={{position: 'fixed', top: '1rem', right: '1rem'}} show={showAlert} variant={alertType} onClose={() => setShowAlert(false)} dismissible>
        <span>{alertMessage}</span>
      </Alert>

    </div>
  );
}

export default App;
