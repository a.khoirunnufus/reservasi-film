import React, { useState, useEffect, useRef } from 'react';
import { Button, Alert } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';

import FilmTable from './components/FilmTable';
import FormModal from './components/FormModal';
import DeleteModal from './components/DeleteModal';

function useDidMount() {
  const didMountRef = useRef(true);
  
  useEffect(() => {
    didMountRef.current = false;
  }, []);
  return didMountRef.current;
};

function App() {
  const didMount = useDidMount();

  // film data
  const [film, setFilm] = useState([]);

  // form field
  const [judul, setJudul] = useState("");
  const [mulaiTayang, setMulaiTayang] = useState("");
  const [selesaiTayang, setSelesaiTayang] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [updateId, setUpdateId] = useState(0);
  const [deleteId, setDeleteId] = useState(0);

  // form modal
  const [showFormModal, setShowFormModal] = useState(false);
  const [formType, setFormType] = useState('');
  const handleCloseFormModal = () => {
    setJudul('');
    setMulaiTayang('');
    setSelesaiTayang('');
    setKeterangan('');
    setFormType('');
    setShowFormModal(false);
  };

  // delete modal
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const handleCloseDeleteModal = () => setShowDeleteModal(false);
  const handleShowDeleteModal = () => setShowDeleteModal(true);

  // alert
  const [showAlert, setShowAlert] = useState(false);
  const [alertType, setAlertType] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  
  useEffect(() => {
    if(didMount) {
      // action when mount
      updateFilmData();
    } 
  });
  
  const updateFilmData = () => {
    fetchFilm()
    .then(data => {
      setFilm(data.data);
    }) 
    .catch(err => {
      console.error(err);
    });
  }

  const fetchFilm = () => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/film')
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  const fetchSingleFilm = (id) => {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:8000/api/film/'+id)
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err));
    })
  }

  // insert film
  const handleShowFormInsertModal = () => {
    setFormType('insert');
    setShowFormModal(true);
  }
  const insertFilm = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/film',
        {
          method: 'POST',
          body: JSON.stringify({judul, mulai_tayang: mulaiTayang, selesai_tayang: selesaiTayang, keterangan}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormInsert = () => {
    insertFilm()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menambahkan Data');
        setShowAlert(true);

        // update film data
        updateFilmData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menambahkan Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // update film
  const handleShowFormUpdateModal = (id) => {
    fetchSingleFilm(id)
      .then(data => {
        // set form data
        setJudul(data.data.judul);
        setMulaiTayang(data.data.mulai_tayang);
        setSelesaiTayang(data.data.selesai_tayang);
        setKeterangan(data.data.keterangan);
        setUpdateId(id);
        setFormType('update');

        // show form
        setShowFormModal(true);
      })
      .catch(err => console.error(err))
  }
  const updateFilm = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/film/'+updateId,
        {
          method: 'PUT',
          body: JSON.stringify({judul, mulai_tayang: mulaiTayang, selesai_tayang: selesaiTayang, keterangan}),
          headers: {
              'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormUpdate = () => {
    updateFilm()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Mengupdate Data');
        setShowAlert(true);

        // update film data
        updateFilmData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Mengupdate Data');
        setShowAlert(true);
      });
    handleCloseFormModal();
  }

  // delete film  
  const deleteFilm = () => {
    return new Promise((resolve, reject) => {
      fetch(
        'http://localhost:8000/api/film/'+deleteId,
        {
          method: 'DELETE'
        })
        .then(res => res.json())
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    })
  }
  const handleFormDelete = () => {
    deleteFilm()
      .then(data => {
        setAlertType('success');
        setAlertMessage('Berhasil Menghapus Data');
        setShowAlert(true);

        // update film data
        updateFilmData();
      })
      .catch(err => {
        setAlertType('danger');
        setAlertMessage('Gagal Menghapus Data');
        setShowAlert(true);
      });
    handleCloseDeleteModal();
  }

  return (
    <div className="App">
      <h3 className="mb-3"><u>Film</u></h3>
      
      <Button variant="primary mb-3 d-block ms-auto" onClick={handleShowFormInsertModal}>
        <FaPlus /> Tambah
      </Button>

      <FilmTable 
        film={film}
        handleShowFormUpdateModal={handleShowFormUpdateModal}
        handleShowDeleteModal={handleShowDeleteModal}
        setDeleteId={setDeleteId}
      />

      <FormModal 
        showFormModal={showFormModal}
        handleCloseFormModal={handleCloseFormModal}
        formType={formType}
        judul={judul}
        selesaiTayang={selesaiTayang}
        mulaiTayang={mulaiTayang}
        keterangan={keterangan}
        setJudul={setJudul}
        setMulaiTayang={setMulaiTayang}
        setSelesaiTayang={setSelesaiTayang}
        setKeterangan={setKeterangan}
        handleFormInsert={handleFormInsert}
        handleFormUpdate={handleFormUpdate}
      />

      <DeleteModal 
        showDeleteModal={showDeleteModal}
        handleCloseDeleteModal={handleCloseDeleteModal}
        handleFormDelete={handleFormDelete}
      />

      <Alert style={{position: 'fixed', top: '1rem', right: '1rem'}} show={showAlert} variant={alertType} onClose={() => setShowAlert(false)} dismissible>
        <span>{alertMessage}</span>
      </Alert>

    </div>
  );
}

export default App;
