import { Table, Button } from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';

const FilmTable = (props) => {
    const {
        film,
        handleShowFormUpdateModal,
        handleShowDeleteModal,
        setDeleteId
    } = props;

    return (
        <Table striped bordered hover style={{width: '100%'}}>
            <thead>
                <tr>
                <th style={{textAlign: 'center', width: '30px'}}>#</th>
                <th>Judul</th>
                <th>Mulai Tayang</th>
                <th>Selesai Tayang</th>
                <th>Keterangan</th>
                <th style={{textAlign: 'center', width: '90px'}}>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {
                film.length > 0 &&
                    film.map((item, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.judul}</td>
                        <td>{item.mulai_tayang}</td>
                        <td>{item.selesai_tayang}</td>
                        <td>{item.keterangan}</td>
                        <td>
                        <Button 
                            onClick={() => handleShowFormUpdateModal(item.id)} 
                            className="me-2" 
                            variant="warning" 
                            size="sm"
                        ><FaEdit /></Button>
                        
                        <Button onClick={() => {
                            handleShowDeleteModal();
                            setDeleteId(item.id);
                            }} 
                            variant="danger" 
                            size="sm"
                        ><FaTrash /></Button>
                        </td>
                    </tr>
                    ))
                }
            </tbody>
        </Table>
    )
}

export default FilmTable;