import { Form, Modal, Button } from 'react-bootstrap';

const FormModal = (props) => {
    const {
      showFormModal,
      handleCloseFormModal,
      formType,
      judul,
      mulaiTayang,
      selesaiTayang,
      keterangan,
      setJudul,
      setSelesaiTayang,
      setMulaiTayang,
      setKeterangan,
      handleFormInsert,
      handleFormUpdate
    } = props;
  
    return (
      <Modal show={showFormModal} onHide={handleCloseFormModal}>
        <Modal.Header closeButton>
          <Modal.Title>{ formType === 'insert' ? 'Tambah' : 'Update'} Film</Modal.Title>
        </Modal.Header>
  
        <Modal.Body>
          
        <Form>
          <Form.Group className="mb-3" controlId="formJudul">
            <Form.Label>Judul</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Masukkan Judul"
              value={judul}
              onChange={(e) => setJudul(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formMulaiTayang">
            <Form.Label>Mulai Tayang</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Contoh: 2021-12-30 10:00:00" 
              value={mulaiTayang}
              onChange={(e) => setMulaiTayang(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formSelesaiTayang">
            <Form.Label>Selesai Tayang</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Contoh: 2021-12-30 12:00:00" 
              value={selesaiTayang}
              onChange={(e) => setSelesaiTayang(e.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formKeterangan">
            <Form.Label>Keterangan</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Masukkan Keterangan"
              value={keterangan}
              onChange={(e) => setKeterangan(e.target.value)}
            />
          </Form.Group>
        </Form>
  
        </Modal.Body>
  
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseFormModal}>Batal</Button>
          { 
            formType === 'insert' ? 
              <Button variant="primary" onClick={handleFormInsert}>Tambah</Button>
              : <Button variant="warning" onClick={handleFormUpdate}>Update</Button>
          }
        </Modal.Footer>
      </Modal>
    )
}

export default FormModal;